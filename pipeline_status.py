#!/usr/bin/env python

import json
import requests
import sys
import sched, time

token = "BnvtBtdxo_BV4y3yk3ux"
REPO = 'montao%2Fforbasile'
s = sched.scheduler(time.time, time.sleep)

def fetch_from_gitlab(token, endpoint, **kwargs):
    url = 'https://gitlab.com/api/v4' + endpoint
    response = requests.get(url, headers={'PRIVATE-TOKEN': token}, params=kwargs)
    output = response.json()[0]["status"]
    if output != "running":
        print(output)
        sys.exit()


def write_result(token, name, endpoint):
    result = fetch_from_gitlab(token, endpoint)


def do_something(sc):
    print "Checking pipeline status"
    write_result(token, 'project', '/projects/%s/pipelines' % REPO)
    s.enter(1, 1, do_something, (sc,))


if __name__ == '__main__':
    s.enter(1, 1, do_something, (s,))
    s.run()